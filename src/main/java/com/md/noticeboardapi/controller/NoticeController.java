package com.md.noticeboardapi.controller;

import com.md.noticeboardapi.model.NoticeItem;
import com.md.noticeboardapi.model.NoticeRequest;
import com.md.noticeboardapi.model.NoticeResponse;
import com.md.noticeboardapi.model.NoticeTitleChangeRequest;
import com.md.noticeboardapi.service.NoticeService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequiredArgsConstructor
@RequestMapping("/v1/notice")
public class NoticeController {
    private final NoticeService noticeService;

    @PostMapping("/post")
    public String setNotice(@RequestBody NoticeRequest request) {
        noticeService.setNotice(request);

        return "ok";
    }

    @GetMapping("/all")
    public List<NoticeItem> getNotices(){
        return noticeService.getNotices();
    }

    @GetMapping("/detail/{id}")
    public NoticeResponse getNotice(@PathVariable long id){
        return noticeService.getNotice(id);
    }

    @PutMapping("/title/{id}")
    public String putNotice(@PathVariable long id, @RequestBody NoticeTitleChangeRequest request){
        noticeService.putNoticeTitle(id, request);

        return "ok";
    }

    @DeleteMapping("/del/{id}")
    public String delNotice(@PathVariable long id){
        noticeService.delNotice(id);

        return "ok";
    }
}
