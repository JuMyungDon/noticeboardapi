package com.md.noticeboardapi.repository;

import com.md.noticeboardapi.entity.Notice;
import org.springframework.data.jpa.repository.JpaRepository;

public interface NoticeRepository extends JpaRepository<Notice, Long> {
}
