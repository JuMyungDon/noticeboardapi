package com.md.noticeboardapi.model;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
public class NoticeResponse {
    private Long id;
    private String title;
    private String name;
    private LocalDateTime postDate;
    private Integer hits;
}
