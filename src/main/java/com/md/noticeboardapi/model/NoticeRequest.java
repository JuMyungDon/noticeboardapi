package com.md.noticeboardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeRequest {
    private String title;
    private String name;
    private Integer hits;
}
