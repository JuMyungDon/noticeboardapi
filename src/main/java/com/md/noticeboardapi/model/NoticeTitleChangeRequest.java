package com.md.noticeboardapi.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class NoticeTitleChangeRequest {
    private String title;
}
