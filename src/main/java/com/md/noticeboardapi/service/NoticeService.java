package com.md.noticeboardapi.service;

import com.md.noticeboardapi.entity.Notice;
import com.md.noticeboardapi.model.NoticeItem;
import com.md.noticeboardapi.model.NoticeRequest;
import com.md.noticeboardapi.model.NoticeResponse;
import com.md.noticeboardapi.model.NoticeTitleChangeRequest;
import com.md.noticeboardapi.repository.NoticeRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class NoticeService {
    private final NoticeRepository noticeRepository;

    public void setNotice(NoticeRequest request) {
        Notice addData = new Notice();
        addData.setTitle(request.getTitle());
        addData.setName(request.getName());
        addData.setPostDate(LocalDateTime.now());
        addData.setHits(request.getHits());

        noticeRepository.save(addData);
    }

    public List<NoticeItem> getNotices() {
        List<Notice> originList = noticeRepository.findAll();

        List<NoticeItem> result = new LinkedList<>();

        for (Notice notice : originList) {
            NoticeItem addItem = new NoticeItem();
            addItem.setId(notice.getId());
            addItem.setTitle(notice.getTitle());
            addItem.setName(notice.getName());
            addItem.setPostDate(notice.getPostDate());
            addItem.setHits(notice.getHits());

            result.add(addItem);
        }

        return result;
    }

    public NoticeResponse getNotice(long id) {
        Notice originData = noticeRepository.findById(id).orElseThrow();

        NoticeResponse response = new NoticeResponse();
        response.setId(originData.getId());
        response.setTitle(originData.getTitle());
        response.setName(originData.getName());
        response.setPostDate(originData.getPostDate());
        response.setHits(originData.getHits());

        return response;
    }

    public void putNoticeTitle(long id, NoticeTitleChangeRequest request){
        Notice originData = noticeRepository.findById(id).orElseThrow();
        originData.setTitle(request.getTitle());

        noticeRepository.save(originData);
    }

    public void delNotice(long id){
        noticeRepository.deleteById(id);
    }
}
